import React from 'react'
import {Text, ImageBackground, View, TouchableHighlight, StyleSheet} from 'react-native'

export default class Carteirinha extends React.Component {

    render() {

        const {navigate} = this.props.navigation;

        return (
            <ImageBackground
            style={{
                backgroundColor: '#ccc',
                flex: 1,
                resizeMode: 'center',
                position: 'absolute',
                width: '100%',
                height: '100%',
                justifyContent: 'center',
            }}
            source={require('./../images/Prancheta_5.png')}
            >
                <View style={{flex: 1}}>
                    <View>
                        <TouchableHighlight style={styles.botao}
                                            onPress={() => { navigate('CarteirinhaScreen') }}>
                            <View style={{flexDirection: "row"}}>
                                <Text style={{marginTop: 15, marginLeft: 5}}>Black</Text>
                                <Text style={{marginTop: 6, position: 'absolute', right: 3, fontSize: 24}}> > </Text>
                            </View>
                        </TouchableHighlight>
                    </View>
                    <View>
                        <TouchableHighlight style={styles.botao}
                                            onPress={() => { navigate('CarteirinhaScreen') }}>
                            <View style={{flexDirection: "row"}}>
                                <Text style={{marginTop: 15, marginLeft: 5}}>Dakota</Text>
                                <Text style={{marginTop: 6, position: 'absolute', right: 3, fontSize: 24}}> > </Text>
                            </View>
                        </TouchableHighlight>
                    </View>
                </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    botao: {
        height: 50,
        width: '100%',
        borderWidth: 1,
        borderTopWidth: 0,
        backgroundColor: 'rgba(255, 255, 255, 0.5)'
    }
});
