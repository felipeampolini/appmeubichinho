import React from 'react'
import {Text, ImageBackground, View, TouchableHighlight} from 'react-native'
import {Switch} from 'react-native-paper';

export default class Configuracoes extends React.Component {

    state = {
        isSwitchOn: false,
    };

    render() {
        const {isSwitchOn} = this.state;
        const {navigate} = this.props.navigation;
        return (
            <ImageBackground
                style={{
                    backgroundColor: '#ccc',
                    flex: 1,
                    resizeMode: 'center',
                    position: 'absolute',
                    width: '100%',
                    height: '100%',
                    justifyContent: 'center',
                }}
                source={require('./../images/Prancheta_14.png')}
            >
                <View style={{flex: 1}}>
                    <View>
                        <TouchableHighlight style={{height: 50, width: '100%', borderWidth: 1, borderBottomWidth: 0}}
                            onPress={() => { navigate('EditarPerfilScreen') }}>
                            <View style={{flexDirection: "row"}}>
                                <Text style={{marginTop: 15, marginLeft: 5}}>Editar Perfil</Text>
                                <Text style={{marginTop: 6, position: 'absolute', right: 3, fontSize: 24}}> > </Text>
                            </View>
                        </TouchableHighlight>
                    </View>
                    <View>
                        <TouchableHighlight style={{height: 50, width: '100%', borderWidth: 1, borderBottomWidth: 0}}>
                            <View>
                                <Text style={{marginTop: 15, marginLeft: 5}}>Backup</Text>
                                <Switch style={{marginTop: 10, position: 'absolute', right: 3, fontSize: 24}}
                                        value={isSwitchOn}
                                        color={'green'}
                                        onValueChange={() => {
                                            this.setState({isSwitchOn: !isSwitchOn});
                                        }
                                        }
                                />
                            </View>
                        </TouchableHighlight>
                    </View>
                    <View>
                        <TouchableHighlight style={{height: 50, width: '100%', borderWidth: 1, borderBottomWidth: 0}}
                            onPress={() => { navigate('LembretesScreen') }}>
                            <View>
                                <Text style={{marginTop: 15, marginLeft: 5}}>Lembretes</Text>
                                <Text style={{marginTop: 6, position: 'absolute', right: 3, fontSize: 24}}> > </Text>
                            </View>
                        </TouchableHighlight>
                    </View>
                    <View>
                        <TouchableHighlight style={{height: 50, width: '100%', borderWidth: 1, borderBottomWidth: 0}}
                            onPress={() => { navigate('QuemSomosScreen') }}>
                            <View>
                                <Text style={{marginTop: 15, marginLeft: 5}}>Quem somos</Text>
                                <Text style={{marginTop: 6, position: 'absolute', right: 3, fontSize: 24}}> > </Text>
                            </View>
                        </TouchableHighlight>
                    </View>
                    <View>
                        <TouchableHighlight style={{height: 50, width: '100%', borderWidth: 1, borderBottomWidth: 0}}>
                            <View>
                                <Text style={{marginTop: 15, marginLeft: 5}}>Indique</Text>
                                <Text style={{marginTop: 6, position: 'absolute', right: 3, fontSize: 24}}> > </Text>
                            </View>
                        </TouchableHighlight>
                    </View>
                    <View>
                        <TouchableHighlight style={{height: 50, width: '100%', borderWidth: 1, borderBottomWidth: 0}}>
                            <View>
                                <Text style={{marginTop: 15, marginLeft: 5}}>Avaliar App</Text>
                                <Text style={{marginTop: 6, position: 'absolute', right: 3, fontSize: 24}}> > </Text>
                            </View>
                        </TouchableHighlight>
                    </View>
                    <View>
                        <TouchableHighlight style={{height: 50, width: '100%', borderWidth: 1}}
                                            onPress={() => { navigate('LoginScreen') }}>
                            <View>
                                <Text style={{marginTop: 15, marginLeft: 5}}>Desconectar</Text>
                                <Text style={{marginTop: 6, position: 'absolute', right: 3, fontSize: 24}}> > </Text>
                            </View>
                        </TouchableHighlight>
                    </View>
                </View>
            </ImageBackground>
        )
    }
}
