import React from 'react'
import {View, Text, Image, StyleSheet, Dimensions, ImageBackground, ScrollView, TouchableHighlight, Modal} from 'react-native'
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import {LocaleConfig} from 'react-native-calendars';

//calendar font https://github.com/wix/react-native-calendars
//
LocaleConfig.locales['br'] = {
    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun', 'Jul','Ago','Set','Out','Nov','Dez'],
    dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb'],
    today: 'Hoje'
};
LocaleConfig.defaultLocale = 'br';

export default class Home extends React.Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible, dateString) {
        this.setState({modalVisible: visible});
    }

    render() {
        const {navigate} = this.props.navigation;
        return(
            <ImageBackground
            style={{
                backgroundColor: '#ccc',
                flex: 1,
                resizeMode: 'center',
                position: 'absolute',
                width: '100%',
                height: '100%',
                justifyContent: 'center',
            }}
            source={require('./../images/Prancheta_17.png')}
            >
                <ScrollView>
                    <View>
                        <Modal
                            animationType="slide"
                            transparent={true}
                            visible={this.state.modalVisible}
                            onRequestClose={() => {
                                Alert.alert('Modal has been closed.');
                            }}>
                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'center',
                                alignItems: 'center'}}>
                                <View style={{
                                    width: '100%',
                                    height: 300,
                                    backgroundColor: 'white'}}>
                                    <TouchableHighlight
                                        onPress={() => {
                                            this.setModalVisible(!this.state.modalVisible);
                                        }}>
                                        <Text>X</Text>
                                    </TouchableHighlight>

                                    <Text>Hello World!</Text>
                                </View>
                            </View>
                        </Modal>
                        <Text style={styles.textoBichinho}>Como está o seu bichinho hoje?</Text>
                        <View style={styles.grupo_imagem}>
                            <TouchableHighlight onPress={() => { navigate('EditarBichinhoScreen') }}>
                                <View style={styles.elemento_imagem}>
                                    <Image
                                        style={styles.imagemBichinho}
                                        source={{uri: 'https://t2.ea.ltmcdn.com/pt/images/3/9/6/img_meu_gatinho_chora_muito_e_normal_21693_600.jpg'}}
                                    />
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight onPress={() => { navigate('CadastraBichinhoScreen') }}>
                                <View style={styles.elemento_imagem}>
                                    <Image
                                        style={styles.imagemBichinho}
                                        source={{uri: 'http://icons.iconarchive.com/icons/iconsmind/outline/512/Add-icon.png'}}
                                    />
                                </View>
                            </TouchableHighlight>
                        </View>
                        <View style={{marginTop: 10, backgroundColor: 'rgba(255, 255, 255, 0.2)'}}>
                            <Calendar onDayPress={(dateString) => this.setModalVisible(true, dateString)}

                                // Specify style for calendar container element. Default = {}
                                style={{
                                    borderWidth: 1,
                                    borderColor: 'gray',
                                    height: 350
                                }}

                                // Specify theme properties to override specific styles for calendar parts. Default = {}
                                theme={{
                                    backgroundColor: '',
                                    calendarBackground: 'rgba(255, 255, 255, 0.2)',
                                    textSectionTitleColor: '#b6c1cd',
                                    selectedDayBackgroundColor: '#00adf5',
                                    selectedDayTextColor: '#ffffff',
                                    todayTextColor: '#00adf5',
                                    dayTextColor: '#2d4150',
                                    textDisabledColor: '#d9e1e8',
                                    dotColor: '#00adf5',
                                    selectedDotColor: '#ffffff',
                                    arrowColor: '#4C2104',
                                    monthTextColor: '#4C2104',
                                    indicatorColor: 'blue',
                                    textDayFontFamily: 'monospace',
                                    textMonthFontFamily: 'monospace',
                                    textDayHeaderFontFamily: 'monospace',
                                    textDayFontWeight: '300',
                                    textMonthFontWeight: 'bold',
                                    textDayHeaderFontWeight: '300',
                                    textDayFontSize: 16,
                                    textMonthFontSize: 16,
                                    textDayHeaderFontSize: 16
                                }}
                            />
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({

    grupo_imagem: {
        flexDirection:"row",
        justifyContent: 'center',
        alignItems: 'center'
    },

    elemento_imagem: {
        flex:1
    },

    textoBichinho: {
        color: '#4C2104',
        fontSize: 30,
        fontWeight: 'bold',
        width: '60%',
        marginTop: 10,
        marginBottom: 10,
        marginLeft: '20%',
        marginRight: '20%',
        textAlign: 'center'
    },
    imagemBichinho: {
        width: Dimensions.get('window').width/3,
        height: Dimensions.get('window').width/3,
        borderRadius: 100,
        marginLeft: 20,
        marginRight: 20,
    }
})
