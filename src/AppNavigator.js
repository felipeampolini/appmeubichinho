import React from 'react'
import { Button, View, Image, StyleSheet, Icon} from 'react-native'
import { createStackNavigator } from 'react-navigation-stack'
import { createAppContainer } from 'react-navigation'
import LoginScreen from './screens/LoginScreen'
import MainScreen from './screens/MainScreen'
import RecuperarSenhaScreen from './screens/RecuperarSenhaScreen'
import CadastroScreen from './screens/CadastroScreen'
import EditarPerfilScreen from './screens/EditarPerfilScreen'
import LembretesScreen from './screens/LembretesScreen'
import QuemSomosScreen from './screens/QuemSomosScreen'
import CarteirinhaScreen from './screens/CarteirinhaScreen'
import CadastraBichinhoScreen from './screens/CadastraBichinhoScreen'
import EditarBichinhoScreen from './screens/EditarBichinhoScreen'


const stack = createStackNavigator({
    LoginScreen: {
        screen: () => <LoginScreen navigation={this.navigation} />,
        navigationOptions: {
            header: null,
        }
    },
    CadastroScreen: {
        screen: CadastroScreen,
        navigationOptions: {
            header: null,
        }
    },
    RecuperarSenhaScreen: {
        screen: RecuperarSenhaScreen,
        navigationOptions: ({navigation}) => ({
            headerLeft: ( <Button title='<' onPress={ () => { navigation.goBack() } }  /> ),
            title: 'Recuperar senha',
            headerStyle: {
                backgroundColor: 'white',
                textTransform: 'uppercase',
                fontWeight: 'bold'
            },
            headerTintColor: '#4C2104'
        })
    },
    MainScreen: {
        screen: MainScreen,
        navigationOptions: () => ({
            headerLeft: (
                <Image
                    style={styles.icone_bichinho}
                    source={{uri: 'http://simpleicon.com/wp-content/uploads/user1.png'}}
                />
                ),
            title: 'MEU BICHINHO',
            titleStyle: {
                fontWeight: 'bold'
            },
            headerStyle: {
                backgroundColor: 'white',
                textTransform: 'uppercase',
            },
            headerTintColor: '#4C2104'
        })
    },
    CarteirinhaScreen: {
        screen: CarteirinhaScreen,
        navigationOptions: {
            header: null,
        }
    },
    CadastraBichinhoScreen: {
        screen: CadastraBichinhoScreen,
        navigationOptions: {
            header: null,
        }
    },
    EditarBichinhoScreen: {
        screen: EditarBichinhoScreen,
        navigationOptions: {
            header: null,
        }
    },
    EditarPerfilScreen: {
        screen: EditarPerfilScreen,
        navigationOptions: {
            header: null,
        }
    },
    LembretesScreen: {
        screen: LembretesScreen,
        navigationOptions: {
            header: null,
        }
    },
    QuemSomosScreen: {
        screen: QuemSomosScreen,
        navigationOptions: {
            header: null,
        }
    }
})

const AppNavigator = createAppContainer(stack);

export default AppNavigator;

const styles = StyleSheet.create({
    texto_Topo: {
    textAlign: 'center'
    },
    icone_bichinho: {
        width: 45,
        height: 45,
        borderRadius: 100,
        marginLeft: 5
    }
})
