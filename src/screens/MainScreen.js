import React from 'react'
import { Text, StyleSheet, Image } from 'react-native'
import Publicacoes from '../components/Publicacoes'
import Carteirinha from '../components/Carteirinha'
import Configuracoes from '../components/Configuracoes'
import Home from '../components/Home'
// import CamComponent from '../components/CamComponent'
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export default createMaterialBottomTabNavigator({

    Home: {
        screen: Home,
        navigationOptions: {
            tabBarIcon:({tintColor}) => (
                <Image source={require('../images/Prancheta-a2.png')} style={[styles.icon, {tintColor: tintColor}]} />
            )
        }
    },
    Publicacoes: {
        screen: Publicacoes,
        navigationOptions: {
            tabBarLabel: 'Publicacoes',
            tabBarIcon:({tintColor}) => (
                <Image source={require('../images/Prancheta-a4.png')} style={[styles.icon, {tintColor: tintColor}]} />
            )
        }
    },
    Carteirinha: {
        screen: Carteirinha,
        navigationOptions: {
            tabBarLabel: 'Carteirinha',
            tabBarIcon:({tintColor}) => (
                <Image source={require('../images/Prancheta-a5.png')} style={[styles.icon, {tintColor: tintColor}]} />
            )
        }
    },
    Configuracoes: {
        screen: Configuracoes,
        navigationOptions: {
            tabBarLabel: 'Configuracoes',
            tabBarIcon:({tintColor}) => (
                <Image source={require('../images/Prancheta-a6.png')} style={[styles.icon, {tintColor: tintColor}]} />
            )
        }
    }
}, {
    initialRouteName: 'Home',
    activeColor: 'grey',
    inactiveColor: 'lightgrey',
    barStyle: {backgroundColor: '#fff'}
})

const styles = StyleSheet.create({
    icon: {
        width: '100%',
        height: '100%',
    }
});
