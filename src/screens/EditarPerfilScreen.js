import React from 'react'
import { View, PixelRatio, TextInput, TouchableHighlight, StyleSheet, Text, Image, TouchableOpacity, ScrollView, ImageBackground} from 'react-native';
import { withNavigation } from 'react-navigation';
import ImagePicker from "react-native-image-picker";
import AsyncStorage from "@react-native-community/async-storage";

export default class EditarPerfilScreen extends React.Component {

    constructor(props) {
        super(props);
        this.selectPhoto = this.selectPhoto.bind(this);

        this.state = {
            urlImage: null,
            nome: null,
            email: null,
            telefone: null,
            senha: null
        }
    }

    componentDidMount() {
        this.getData();
    }

    getData = async () => {
        try {
            const dados = await AsyncStorage.getItem('@storage_');
            let aux = JSON.parse(dados);
            this.setState(aux);
        } catch (e) {
            // error reading value
        }
    };

    storeData = async () => {
        try {
            // alert(JSON.stringify(this.state));
            await AsyncStorage.setItem('@storage_', JSON.stringify(this.state))
        } catch (e) {
            alert(e);
        }
        this.props.navigation.replace('MainScreen')
    };

    selectPhoto() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };

        ImagePicker.showImagePicker(options, response => {
            console.log('Response = ', response)

            if (response.didCancel) {
                console.log('Usuário cancelou!')
            } else if (response.error) {
                console.log("ImagePicker erro: "+response.error)
            } else {
                let source = {uri: response.uri};

                this.setState({imageSource: source})
                console.log('ImageSource: '+this.state.urlImage)
            }
        })
    }

    render() {
        return (
            <ImageBackground
                style={{
                    backgroundColor: '#ccc',
                    flex: 1,
                    resizeMode: 'center',
                    position: 'absolute',
                    width: '100%',
                    height: '100%',
                    justifyContent: 'center',
                }}
                source={require('../images/Prancheta_16.png')}
            >
                <ScrollView>
                    <View>
                        <View style={{flex: 1, flexDirection:'row', justifyContent: 'center'}}>
                            <TouchableOpacity onPress={this.selectPhoto.bind(this)}>
                                <View style={[styles.img, styles.imgContainer, {alignItems:'center', marginTop: 30, marginBottom: 30}]}>
                                    {this.state.urlImage === null ?
                                        <Text>
                                            Selecione uma foto!
                                        </Text> :
                                        (<Image style={styles.img} source={{uri: this.state.urlImage}}/>)
                                    }
                                </View>
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.TextoInicial}>Nome</Text>
                        <TextInput
                            editable = {true}
                            autoCapitalize = 'none'
                            style={styles.input}
                            value={this.state.nome}
                            onChangeText={(inputValue) => this.setState({nome: inputValue})}
                        >
                        </TextInput>

                        <Text style={styles.TextoInicial}>Email</Text>
                        <TextInput
                            editable = {true}
                            autoCapitalize = 'none'
                            style={styles.input}
                            value={this.state.email}
                            onChangeText={(inputValue) => this.setState({email: inputValue})}
                        >
                        </TextInput>

                        <Text style={styles.TextoInicial}>Número de telefone</Text>
                        <TextInput
                            editable = {true}
                            autoCapitalize = 'none'
                            style={styles.input}
                            keyboardType={'numeric'}
                            value={this.state.telefone}
                            onChangeText={(inputValue) => this.setState({telefone: inputValue})}
                        >
                        </TextInput>

                        <TouchableHighlight onPress={() => {
                            this.storeData()
                        }} style={styles.cadastrar}>
                            <Text style={styles.cadastrarText}>Salva</Text>
                        </TouchableHighlight>
                    </View>
                </ScrollView>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({

    imgContainer: {
        borderColor: '#000',
        borderWidth: 1 / PixelRatio.get(),
        justifyContent: 'center',
        alignItems: 'center'

    },
    img: {
        borderRadius: 75,
        width: 150,
        height: 150
    },
    input: {
        marginLeft: '10%',
        marginRight: '10%',
        marginBottom: 10,
        width: '80%',
        height: 50,
        paddingBottom: -20,
        marginTop: -20,
        borderBottomWidth: 1,
        borderBottomColor: 'black'
    },
    TextoInicial: {
        width: '80%',
        marginLeft: '10%',
        marginRight: '10%',
    },
    cadastrar: {
        marginLeft: '10%',
        marginRight: '10%',
        marginTop: 20,
        padding: 20,
        width: '80%',
        backgroundColor: 'grey',
        borderRadius: 5,
        borderWidth: 1,
        borderBottomColor: 'blue',
    },
    cadastrarText: {
        color: 'white',
        textAlign: 'center'
    },
    flexConta:{
        marginLeft: '10%',
        marginTop: 30,
        flex: 1,
        flexDirection: 'row'
    },
    possuiConta: {
        color: 'grey',
    },
    login: {
        textAlign: 'right',
        marginLeft: '10%',
        color: 'brown'
    }
});
