import React from 'react'
import {
    View,
    PixelRatio,
    TextInput,
    TouchableHighlight,
    StyleSheet,
    Text,
    Image,
    TouchableOpacity,
    ScrollView, ImageBackground
} from 'react-native';
import { withNavigation } from 'react-navigation'
import ImagePicker from "react-native-image-picker";
import firebase from 'react-native-firebase';

export class CadastroScreen extends React.Component {

    constructor(props) {
        super(props);
        this.selectPhoto = this.selectPhoto.bind(this);

        this.state = {
            urlImage: null,
            nome: null,
            email: null,
            telefone: null,
            senha: null
        }
    }

    create = (email, senha) => {

        try {
          firebase
              .auth()
              .createUserWithEmailAndPassword(email, senha)
              .then(alert('usuario criado com sucesso!'));
              this.props.navigation.navigate('LoginScreen');
    } catch (error) {
          alert(error);
        }
      };

    selectPhoto() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };

        ImagePicker.showImagePicker(options, response => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('Usuário cancelou!');
            } else if (response.error) {
                console.log("ImagePicker erro: "+response.error);
            } else {
                let source = {uri: response.uri};
                console.log(source);
                this.setState({urlImage: source.uri});
                // alert('ImageSource: '+ JSON.stringify(this.state.urlImage));
            }
        })
    }

    async verificaSenha(){
        if(this.informacao.confirmaSenha !== this.state.senha){
            alert("Senhas incompativeis!");
        }
    }

    render() {
        return (
            <ImageBackground
                style={{
                    backgroundColor: '#ccc',
                    flex: 1,
                    resizeMode: 'center',
                    position: 'absolute',
                    width: '100%',
                    height: '100%',
                    justifyContent: 'center',
                }}
                source={require('./../images/bg-login.png')}
            >
                <ScrollView>
                    <View>
                        <View style={{flex: 1, flexDirection:'row', justifyContent: 'center'}}>
                            <TouchableOpacity onPress={this.selectPhoto.bind(this)}>
                                <View style={[styles.img, styles.imgContainer, {alignItems:'center', marginTop: 30, marginBottom: 30}]}>
                                    {this.state.urlImage === null ?
                                        <Text>
                                            Selecione uma foto!
                                        </Text> :
                                        (<Image style={styles.img} source={{uri: this.state.urlImage}}/>)
                                    }
                                </View>
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.TextoInicial}>Nome</Text>
                        <TextInput
                            editable = {true}
                            autoCapitalize = 'none'
                            style={styles.input}
                            value={this.state.nome}
                            onChangeText={(inputValue) => this.setState({nome: inputValue})}
                        >
                        </TextInput>

                        <Text style={styles.TextoInicial}>Email</Text>
                        <TextInput
                            editable = {true}
                            autoCapitalize = 'none'
                            style={styles.input}
                            onChangeText={(email) => {this.state.email = email}}
                            value={this.state.email}
                        >
                        </TextInput>

                        <Text style={styles.TextoInicial}>Telefone</Text>
                        <TextInput
                            editable = {true}
                            autoCapitalize = 'none'
                            style={styles.input}
                            keyboardType={'numeric'}
                            value={this.state.telefone}
                            onChangeText={(inputValue) => this.setState({telefone: inputValue})}
                        >
                        </TextInput>

                        <Text style={styles.TextoInicial}>Senha</Text>
                        <TextInput
                            secureTextEntry= {true}
                            editable = {true}
                            autoCapitalize = 'none'
                            style={styles.input}
                            onChangeText={(senha) => {this.state.senha = senha}}
                            value={this.state.senha}
                            
                        >
                        </TextInput>

                        <TouchableHighlight onPress={() => {this.create(this.state.email, this.state.senha)} } style={styles.cadastrar}>
                            <Text style={styles.cadastrarText}>Registrar</Text>
                        </TouchableHighlight>

                        <View style={styles.flexConta}>
                            <Text style={styles.possuiConta}>Já possui uma conta? Faça o</Text>
                            <TouchableHighlight onPress={() => {this.props.navigation.replace('LoginScreen')}}>
                                <Text style={styles.login}>Login</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground>
        )
    }
}

export default withNavigation(CadastroScreen);

const styles = StyleSheet.create({

    imgContainer: {
        backgroundColor: 'white',
        borderColor: '#000',
        borderWidth: 1 / PixelRatio.get(),
        justifyContent: 'center',
        alignItems: 'center'
    },
    img: {
        borderRadius: 75,
        width: 150,
        height: 150
    },
    input: {
        marginLeft: '10%',
        marginRight: '10%',
        marginBottom: 10,
        width: '80%',
        height: 50,
        paddingBottom: -20,
        marginTop: -20,
        borderBottomWidth: 1,
        borderBottomColor: 'black'
    },
    TextoInicial: {
        width: '80%',
        marginLeft: '10%',
        marginRight: '10%',
    },
    cadastrar: {
        marginLeft: '10%',
        marginRight: '10%',
        marginTop: 20,
        padding: 20,
        width: '80%',
        backgroundColor: 'grey',
        borderRadius: 5,
        borderWidth: 1,
        borderBottomColor: 'blue',
    },
    cadastrarText: {
        color: 'white',
        textAlign: 'center'
    },
    flexConta:{
        marginLeft: '10%',
        marginTop: 30,
        flex: 1,
        flexDirection: 'row'
    },
    possuiConta: {
        color: 'grey',
    },
    login: {
        textAlign: 'right',
        marginLeft: '10%',
        color: '#4C2104'
    }
})
