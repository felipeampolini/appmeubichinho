import React from 'react'
import {Text, ImageBackground, View, TouchableHighlight, StyleSheet, TextInput} from 'react-native'
import { DataTable } from 'react-native-paper';


export default class CarteirinhaScreen extends React.Component {

    render() {

        return (
            <ImageBackground
            style={{
                backgroundColor: '#ccc',
                flex: 1,
                resizeMode: 'center',
                position: 'absolute',
                width: '100%',
                height: '100%',
                justifyContent: 'center',
            }}
            source={require('./../images/Prancheta_5.png')}
            >
                <View style={{flex: 1}}>
                    <View style={styles.flexView}>
                        <Text style={styles.petName}>Black</Text>
                    </View>
                    <View>
                        <Text style={styles.input}>Vacinações</Text>
                    </View>
                    <DataTable style={styles.table}>
                        <DataTable.Header>
                            <DataTable.Title>Medicação</DataTable.Title>
                            <DataTable.Title>Data</DataTable.Title>
                        </DataTable.Header>

                        <DataTable.Row>
                            <DataTable.Cell></DataTable.Cell>
                            <DataTable.Cell></DataTable.Cell>
                        </DataTable.Row>

                        <DataTable.Row>
                            <DataTable.Cell></DataTable.Cell>
                            <DataTable.Cell></DataTable.Cell>
                        </DataTable.Row>
                    </DataTable>
                    <View>
                        <Text style={styles.input}>Não pode comer</Text>
                        <TextInput
                            editable = {true}
                            autoCapitalize = 'none'
                            style={{
                                backgroundColor: 'rgba(255, 255, 255, 0.5)',
                                marginLeft: '10%',
                                marginRight: '10%',
                                marginBottom: 10,
                                width: '80%',
                                height: 35,
                            }}
                        >
                        </TextInput>
                    </View>
                    <View>
                        <Text style={styles.input}>Estresse por</Text>
                        <TextInput
                            editable = {true}
                            autoCapitalize = 'none'
                            style={{
                                backgroundColor: 'rgba(255, 255, 255, 0.5)',
                                marginLeft: '10%',
                                marginRight: '10%',
                                marginBottom: 10,
                                width: '80%',
                                height: 35,
                            }}
                        >
                        </TextInput>
                    </View>
                    <View>
                        <Text style={styles.input}>Alergia a</Text>
                        <TextInput
                            editable = {true}
                            autoCapitalize = 'none'
                            style={{
                                backgroundColor: 'rgba(255, 255, 255, 0.5)',
                                marginLeft: '10%',
                                marginRight: '10%',
                                marginBottom: 10,
                                width: '80%',
                                height: 35,
                            }}
                        >
                        </TextInput>
                    </View>
                    <View>
                        <Text style={styles.input}>Data de cio</Text>
                        <TextInput
                            editable = {true}
                            autoCapitalize = 'none'
                            style={{
                                backgroundColor: 'rgba(255, 255, 255, 0.5)',
                                marginLeft: '10%',
                                marginRight: '10%',
                                marginBottom: 10,
                                width: '80%',
                                height: 35,
                            }}
                        >
                        </TextInput>
                    </View>
                </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    botao: {
        height: 50,
        width: '100%',
        borderWidth: 1,
        borderTopWidth: 0,
        backgroundColor: 'rgba(255, 255, 255, 0.5)'
    },
    input: {
        width: '80%',
        marginTop: 10,
        marginLeft: '10%',
        marginRight: '10%',
    },
    table: {
        width: '80%',
        marginTop: 10,
        marginLeft: '10%',
        marginRight: '10%',
        backgroundColor: 'rgba(255, 255, 255, 0.5)'
    },
    flexView: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    petName: {
        fontSize: 25
    }
});
