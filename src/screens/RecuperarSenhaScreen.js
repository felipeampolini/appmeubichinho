import React from 'react'
import {View, TextInput, TouchableHighlight, StyleSheet, Text, Image, ImageBackground} from 'react-native'
import { withNavigation } from 'react-navigation'

export class RecuperarSenhaScreen extends React.Component {

    render() {
        return (
            <ImageBackground
                style={{
                    backgroundColor: '#ccc',
                    flex: 1,
                    resizeMode: 'center',
                    position: 'absolute',
                    width: '100%',
                    height: '100%',
                    justifyContent: 'center',
                }}
                source={require('./../images/bg-login.png')}
            >
                <View>
                    <View style={styles.flexRecSenha}>
                        <Text style={styles.esqueceuSenha}>Esqueceu sua senha?</Text>
                        <Text style={styles.esqueceuSenhaText}>Por favor, insira um endereço de email</Text>
                        <Text style={styles.esqueceuSenhaText}>de associado. Nós lhe enviaremos um email</Text>
                        <Text style={styles.esqueceuSenhaText}>com link para redefenir sua senha.</Text>
                    </View>

                    <Text style={styles.loginSenha}>Email</Text>
                    <TextInput
                        editable = {true}
                        autoCapitalize = 'none'
                        style={{
                            backgroundColor: 'rgba(255, 255, 255, 0.5)',
                            marginLeft: '10%',
                            marginRight: '10%',
                            marginBottom: 10,
                            width: '80%',
                            height: 35,
                        }}
                    >
                    </TextInput>

                    <TouchableHighlight onPress={() => {this.props.navigation.replace('LoginScreen')}}
                                        style={styles.login}>
                        <View style={styles.flexRecSenha}>
                            <Text style={styles.loginText}>Enviar</Text>
                        </View>
                    </TouchableHighlight>

                </View>
            </ImageBackground>
        )
    }
}

export default withNavigation(RecuperarSenhaScreen);

const styles = StyleSheet.create({
    flexRecSenha: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    esqueceuSenha: {
        fontSize: 25,
        marginBottom: 20
    },
    esqueceuSenhaText:{
        marginLeft: '15%',
        marginRight: '15%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    loginSenha: {
        width: '80%',
        marginTop: 80,
        marginLeft: '10%',
        marginRight: '10%',
    },
    login: {
        marginLeft: '10%',
        marginRight: '10%',
        marginTop: 20,
        padding: 20,
        width: '80%',
        backgroundColor: 'grey',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'grey'
    },
    loginText: {
        color: 'white',
        alignContent: 'center'
    },
});
