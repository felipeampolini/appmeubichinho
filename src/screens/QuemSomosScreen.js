import React from 'react'
import {Text, ImageBackground} from 'react-native'

export default class QuemSomosScreen extends React.Component {

    render() {
        return (
            <ImageBackground
                style={{
                    backgroundColor: '#ccc',
                    flex: 1,
                    resizeMode: 'center',
                    position: 'absolute',
                    width: '100%',
                    height: '100%',
                    justifyContent: 'center',
                }}
                source={require('./../images/Prancheta_14.png')}
            >
                <Text>
                    Design e idéia dos estudantes de Desing
                    Desenvolvimento dos estudastes de Ciência da Computação
                </Text>
            </ImageBackground>
        )
    }
}
