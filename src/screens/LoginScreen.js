import React from 'react'
import {View, TextInput, TouchableHighlight, StyleSheet, Text, ImageBackground, Image, ScrollView} from 'react-native'
import {withNavigation} from 'react-navigation'
import AsyncStorage from "@react-native-community/async-storage";
import firebase from 'react-native-firebase';

export class LoginScreen extends React.Component {

    informações = {
        email: null,
        senha: null
    };

    constructor(props) {

        super(props);

        this.state = {
            urlImage: null,
            nome: null,
            email: null,
            telefone: null,
            senha: null,
            email: '',
            pass: '',
            isAuthenticaded: false
        }
    } 

    loginAuth = async () => {
        const { email, pass } = this.state;
        
        try {const user = await firebase.auth().signInWithEmailAndPassword(email, pass)
        .then(() => this.props.navigation.navigate('MainScreen'));
        alert('Logado com Sucesso');
        } catch(error) {
        alert('Erro no email ou password');
        }
    };

    componentDidMount() {
        this.getData();
    }

    getData = async () => {
        try {
            const dados = await AsyncStorage.getItem('@storage_');
            let aux = JSON.parse(dados);
            this.setState(aux);
        } catch (e) {
            // error reading value
        }
    };

    async verificaEmail() {
        if (this.informações.email !== this.state.email) {
            alert("Email não está registro!");
            this.informações.email = null;
        }
    }

    async verificaCredenciais() {
        if (this.informações.email === this.state.email) {
            if (this.informações.senha === this.state.senha) {
                this.props.navigation.replace('MainScreen');
            } else {
                alert("Informações Incorretas!");
            }
        } else {
            alert("Informações Incorretas!");
        }
    }

    render() {
        return (
            <ImageBackground
                style={{
                    backgroundColor: '#ccc',
                    flex: 1,
                    resizeMode: 'center',
                    position: 'absolute',
                    width: '100%',
                    height: '100%',
                    justifyContent: 'center',
                }}
                source={require('./../images/bg-login.png')}
            >
                <ScrollView>
                    <View>
                        <View
                            style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                            <Image style={{marginTop: 10, width: 150, height: 150}}
                                   source={require('./../images/logo.png')}/>
                            <Image style={{resizeMode: 'contain', width: 150, height: 150}}
                                   source={require('./../images/escrita_logo.png')}/>
                        </View>
                        {/*<Text style={styles.LogoText}>Meu Bichinho</Text>*/}
                        <Text style={styles.loginSenha}>Email</Text>
                        <TextInput
                            editable={true}
                            autoCapitalize='none'
                            style={{
                                backgroundColor: 'lightgrey',
                                marginLeft: '10%',
                                marginRight: '10%',
                                marginBottom: 10,
                                width: '80%',
                                height: 35
                            }}
                            onChangeText={(inputValue) => {
                                this.state.email = inputValue
                            }}
                        >
                        </TextInput>

                        <Text style={styles.loginSenha}>Senha</Text>
                        <TextInput
                            secureTextEntry={true}
                            editable={true}
                            autoCapitalize='none'
                            style={{
                                backgroundColor: 'lightgrey',
                                marginLeft: '10%',
                                marginRight: '10%',
                                width: '80%',
                                height: 35
                            }}
                            onChangeText={(inputValue) => {
                                this.state.pass = inputValue
                            }}
                        >
                        </TextInput>

                        <TouchableHighlight onPress={() => {
                            this.props.navigation.replace('RecuperarSenhaScreen')

                        }}>
                            <Text style={styles.esqueceuSenha}>Esqueceu sua senha?</Text>
                        </TouchableHighlight>

                        <TouchableHighlight onPress={() => {
                            this.loginAuth()
                        }} style={styles.login}>
                            <Text style={styles.loginText}>Login</Text>
                        </TouchableHighlight>

                        <TouchableHighlight onPress={() => {
                            alert("Função não implementada!");
                        }} style={styles.face}>
                            <Text style={styles.faceText}>Login com Facebook</Text>
                        </TouchableHighlight>

                        <View style={styles.flexConta}>
                            <Text style={styles.temConta}>Você não possui uma conta?</Text>
                            <TouchableHighlight onPress={() => {
                                this.props.navigation.replace('CadastroScreen')
                            }}>
                                <Text style={styles.cadastrar}>Cadastre-se</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground>
        )
    }
}

export default withNavigation(LoginScreen);

const styles = StyleSheet.create({
    LogoText: {
        fontSize: 30,
        fontWeight: 'bold',
        width: '33%',
        marginLeft: '33%',
        marginRight: '33%',
    },
    esqueceuSenha: {
        textAlign: 'right',
        marginRight: '10%',
    },
    loginSenha: {
        width: '80%',
        marginLeft: '10%',
        marginRight: '10%',
    },
    login: {
        marginLeft: '10%',
        marginRight: '10%',
        marginTop: 40,
        padding: 20,
        width: '80%',
        backgroundColor: 'grey',
        borderRadius: 5,
    },
    loginText: {
        color: 'white'
    },
    face: {
        marginLeft: '10%',
        marginRight: '10%',
        marginTop: 15,
        padding: 20,
        width: '80%',
        backgroundColor: '#3b5998',
        borderRadius: 5,
    },
    faceText: {
        color: 'white'
    },
    flexConta: {
        marginLeft: '10%',
        marginTop: 30,
        flex: 1,
        flexDirection: 'row'
    },
    temConta: {
        color: 'grey',
    },
    cadastrar: {
        textAlign: 'right',
        marginLeft: '10%',
        color: '#4C2104',
    }
})
