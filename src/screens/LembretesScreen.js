import React from 'react'
import {Text, ImageBackground, View, TouchableHighlight} from 'react-native'
import {Switch} from 'react-native-paper';

export default class Configuracoes extends React.Component {

    state = {
        isSwitchOn1: false,
        isSwitchOn2: false,
    };

    render() {
        const {isSwitchOn1} = this.state;
        const {isSwitchOn2} = this.state;

        return (
            <ImageBackground
                style={{
                    backgroundColor: '#ccc',
                    flex: 1,
                    resizeMode: 'center',
                    position: 'absolute',
                    width: '100%',
                    height: '100%',
                    justifyContent: 'center',
                }}
                source={require('./../images/Prancheta_14.png')}
            >
                <View style={{flex: 1}}>
                    <View>
                        <TouchableHighlight style={{height: 50, width: '100%', borderWidth: 1, borderBottomWidth: 0}}>
                            <View>
                                <Text style={{marginTop: 15, marginLeft: 5}}>Cio</Text>
                                <Switch style={{marginTop: 10, position: 'absolute', right: 3, fontSize: 24}}
                                        value={isSwitchOn1}
                                        color={'green'}
                                        onValueChange={() => {
                                            this.setState({isSwitchOn1: !isSwitchOn1});
                                        }
                                        }
                                />
                            </View>
                        </TouchableHighlight>
                    </View>
                    <View>
                        <TouchableHighlight style={{height: 50, width: '100%', borderWidth: 1}}>
                            <View>
                                <Text style={{marginTop: 15, marginLeft: 5}}>Vermifogo</Text>
                                <Switch style={{marginTop: 10, position: 'absolute', right: 3, fontSize: 24}}
                                        value={isSwitchOn2}
                                        color={'green'}
                                        onValueChange={() => {
                                            this.setState({isSwitchOn2: !isSwitchOn2});
                                        }
                                        }
                                />
                            </View>
                        </TouchableHighlight>
                    </View>
                </View>
            </ImageBackground>
        )
    }
}
