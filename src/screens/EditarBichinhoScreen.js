import React from 'react'
import {
    View,
    PixelRatio,
    TextInput,
    TouchableHighlight,
    StyleSheet,
    Text,
    Image,
    TouchableOpacity,
    ScrollView, ImageBackground
} from 'react-native';
import {withNavigation} from 'react-navigation'
import ImagePicker from "react-native-image-picker";
import RNPickerSelect from 'react-native-picker-select';

export class EditarBichinhoScreen extends React.Component {

    state = {
        imageSource: null
    };

    constructor(props) {
        super(props);
        this.selectPhoto = this.selectPhoto.bind(this);
    }

    selectPhoto() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };

        ImagePicker.showImagePicker(options, response => {
            console.log('Response = ', response)

            if (response.didCancel) {
                console.log('Usuário cancelou!')
            } else if (response.error) {
                console.log("ImagePicker erro: " + response.error)
            } else {
                let source = {uri: response.uri};

                this.setState({imageSource: source})
                console.log('ImageSource: ' + this.state.imageSource)
            }
        })
    }

    render() {
        return (
            <ImageBackground
                style={{
                    backgroundColor: '#ccc',
                    flex: 1,
                    resizeMode: 'center',
                    position: 'absolute',
                    width: '100%',
                    height: '100%',
                    justifyContent: 'center',
                }}
                source={require('./../images/Prancheta_17.png')}
            >
                <ScrollView>
                    <View>
                        <View style={{flex: 1, marginTop: '10%', marginLeft: '10%'}}>
                            <Text style={{fontSize: 20}}>Humor</Text>
                        </View>
                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            marginLeft: '10%',
                            marginTop: '5%',
                            marginRight: '10%'
                        }}>
                            <TouchableOpacity>
                                <View>
                                    <Image style={{width: 45, height: 45, marginLeft: 'auto', marginRight: 'auto'}}
                                           source={require('./../images/Prancheta_28.png')}/>
                                    <Text style={{fontSize: 10, marginLeft: 'auto', marginRight: 'auto'}}>dengoso</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View>
                                    <Image style={{width: 45, height: 45, marginLeft: 'auto', marginRight: 'auto'}}
                                           source={require('./../images/Prancheta_25_1.png')}/>
                                    <Text style={{fontSize: 10, marginLeft: 'auto', marginRight: 'auto'}}>agressivo</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View>
                                    <Image style={{width: 45, height: 45, marginLeft: 'auto', marginRight: 'auto'}}
                                           source={require('./../images/Prancheta_27.png')}/>
                                    <Text style={{fontSize: 10, marginLeft: 'auto', marginRight: 'auto'}}>animado</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View>
                                    <Image style={{width: 45, height: 45, marginLeft: 'auto', marginRight: 'auto'}}
                                           source={require('../images/Prancheta_27_copia.png')}/>
                                    <Text style={{fontSize: 10, marginLeft: 'auto', marginRight: 'auto'}}>cansado</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View>
                                    <Image style={{width: 45, height: 45, marginLeft: 'auto', marginRight: 'auto'}}
                                           source={require('./../images/Prancheta_27_copia_2.png')}/>
                                    <Text style={{fontSize: 10, marginLeft: 'auto', marginRight: 'auto'}}>agitado</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View>
                                    <Image style={{width: 45, height: 45, marginLeft: 'auto', marginRight: 'auto'}}
                                           source={require('./../images/Prancheta_27_copia_3.png')}/>
                                    <Text style={{fontSize: 10, marginLeft: 'auto', marginRight: 'auto'}}>assustado</Text>
                                </View>
                            </TouchableOpacity>

                        </View>

                        <View style={{flex: 1, marginTop: '10%', marginLeft: '10%'}}>
                            <Text style={{fontSize: 20}}>Alimentação</Text>
                        </View>
                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            marginLeft: '10%',
                            marginTop: '5%',
                            marginRight: '10%'
                        }}>
                            <TouchableOpacity>
                                <View>
                                    <Image style={{width: 45, height: 45, marginLeft: 'auto', marginRight: 'auto'}}
                                           source={require('./../images/Prancheta_3_copia.png')}/>
                                    <Text style={{fontSize: 10, marginLeft: 'auto', marginRight: 'auto'}}>pouca ração</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View>
                                    <Image style={{width: 45, height: 45, marginLeft: 'auto', marginRight: 'auto'}}
                                           source={require('./../images/Prancheta_a8.png')}/>
                                    <Text style={{fontSize: 10, marginLeft: 'auto', marginRight: 'auto'}}>muita ração</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View>
                                    <Image style={{width: 45, height: 45, marginLeft: 'auto', marginRight: 'auto'}}
                                           source={require('./../images/Prancheta_a11.png')}/>
                                    <Text style={{fontSize: 10, marginLeft: 'auto', marginRight: 'auto'}}>leite</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View>
                                    <Image style={{width: 45, height: 45, marginLeft: 'auto', marginRight: 'auto'}}
                                           source={require('../images/Prancheta_a9.png')}/>
                                    <Text style={{fontSize: 10, marginLeft: 'auto', marginRight: 'auto'}}>carne</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View>
                                    <Image style={{width: 45, height: 45, marginLeft: 'auto', marginRight: 'auto'}}
                                           source={require('./../images/Prancheta_32.png')}/>
                                    <Text style={{fontSize: 10, marginLeft: 'auto', marginRight: 'auto'}}>frutas</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View>
                                    <Image style={{width: 45, height: 45, marginLeft: 'auto', marginRight: 'auto'}}
                                           source={require('./../images/Prancheta_a10.png')}/>
                                    <Text style={{fontSize: 10, marginLeft: 'auto', marginRight: 'auto'}}>sache</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex: 1, marginTop: '10%', marginLeft: '10%'}}>
                            <Text style={{fontSize: 20}}>Anomalias</Text>
                        </View>
                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            marginLeft: '10%',
                            marginTop: '5%',
                            marginRight: '10%'
                        }}>
                            <TouchableOpacity>
                                <View>
                                    <Image style={{width: 45, height: 45, marginLeft: 'auto', marginRight: 'auto'}}
                                           source={require('./../images/Prancheta_35_copia.png')}/>
                                    <Text style={{fontSize: 10, marginLeft: 'auto', marginRight: 'auto'}}>perdeu pelo</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View>
                                    <Image style={{width: 45, height: 45, marginLeft: 'auto', marginRight: 'auto'}}
                                           source={require('./../images/Prancheta_25_copia_2.png')}/>
                                    <Text style={{fontSize: 10, marginLeft: 'auto', marginRight: 'auto'}}>vômito</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View>
                                    <Image style={{width: 45, height: 45, marginLeft: 'auto', marginRight: 'auto'}}
                                           source={require('./../images/Prancheta_32_copia.png')}/>
                                    <Text style={{fontSize: 10, marginLeft: 'auto', marginRight: 'auto'}}>vermes</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View>
                                    <Image style={{width: 45, height: 45, marginLeft: 'auto', marginRight: 'auto'}}
                                           source={require('./../images/Prancheta_33_copia.png')}/>
                                    <Text style={{fontSize: 10, marginLeft: 'auto', marginRight: 'auto'}}>odor</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View>
                                    <Image style={{width: 45, height: 45, marginLeft: 'auto', marginRight: 'auto'}}
                                           source={require('./../images/Prancheta_34_copia.png')}/>
                                    <Text style={{fontSize: 10, marginLeft: 'auto', marginRight: 'auto'}}>fezes estranhas</Text>
                                </View>
                            </TouchableOpacity>
                        </View>

                        <View style={{flex: 1, marginTop: '10%', marginLeft: '10%'}}>
                            <Text style={{fontSize: 20}}>Anotações adicionais</Text>
                        </View>
                        <TextInput
                            style={{
                                backgroundColor: 'rgba(255, 255, 255, 0.5)',
                                borderRadius: 15,
                                marginRight: '10%',
                                marginLeft: '10%',
                                marginTop: '2%'
                            }}
                            multiline={true}
                            numberOfLines={4}
                        />

                        <TouchableHighlight onPress={() => {
                            this.props.navigation.replace('MainScreen')
                        }} style={styles.cadastrar}>
                            <Text style={styles.cadastrarText}>Salvar</Text>
                        </TouchableHighlight>
                    </View>
                </ScrollView>
            </ImageBackground>
        )
    }
}

export default withNavigation(EditarBichinhoScreen);

const styles = StyleSheet.create({

    imgContainer: {
        backgroundColor: 'white',
        borderColor: '#000',
        borderWidth: 1 / PixelRatio.get(),
        justifyContent: 'center',
        alignItems: 'center'
    },
    img: {
        borderRadius: 75,
        width: 150,
        height: 150
    },
    input: {
        marginLeft: '10%',
        marginRight: '10%',
        marginBottom: 10,
        width: '80%',
        height: 50,
        paddingBottom: -20,
        marginTop: -20,
        borderBottomWidth: 1,
        borderBottomColor: 'black'
    },
    cadastrarSenha: {
        width: '80%',
        marginLeft: '10%',
        marginRight: '10%',
    },
    cadastrar: {
        marginLeft: '10%',
        marginRight: '10%',
        marginTop: 20,
        padding: 20,
        width: '80%',
        backgroundColor: 'grey',
        borderRadius: 5,
        borderWidth: 1,
        borderBottomColor: 'blue',
    },
    cadastrarText: {
        color: 'white',
        textAlign: 'center'
    },
    flexConta: {
        marginLeft: '10%',
        marginTop: 30,
        flex: 1,
        flexDirection: 'row'
    },
    possuiConta: {
        color: 'grey',
    },
    login: {
        textAlign: 'right',
        marginLeft: '10%',
        color: '#4C2104'
    }
})
