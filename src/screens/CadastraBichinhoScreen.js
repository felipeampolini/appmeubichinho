import React from 'react'
import {
    View,
    PixelRatio,
    TextInput,
    TouchableHighlight,
    StyleSheet,
    Text,
    Image,
    TouchableOpacity,
    ScrollView, ImageBackground
} from 'react-native';
import { withNavigation } from 'react-navigation'
import ImagePicker from "react-native-image-picker";
import RNPickerSelect from 'react-native-picker-select';

export class CadastraBichinhoScreen extends React.Component {

    state = {
        imageSource: null
    };

    constructor(props) {
        super(props);
        this.selectPhoto = this.selectPhoto.bind(this);
    }

    selectPhoto() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };

        ImagePicker.showImagePicker(options, response => {
            console.log('Response = ', response)

            if (response.didCancel) {
                console.log('Usuário cancelou!')
            } else if (response.error) {
                console.log("ImagePicker erro: "+response.error)
            } else {
                let source = {uri: response.uri};

                this.setState({imageSource: source})
                console.log('ImageSource: '+this.state.imageSource)
            }
        })
    }

    render() {
        return (
            <ImageBackground
                style={{
                    backgroundColor: '#ccc',
                    flex: 1,
                    resizeMode: 'center',
                    position: 'absolute',
                    width: '100%',
                    height: '100%',
                    justifyContent: 'center',
                }}
                source={require('./../images/Prancheta_16.png')}
            >
                <ScrollView>
                    <View>
                        <View style={{flex: 1, flexDirection:'row', justifyContent: 'center'}}>
                            <TouchableOpacity onPress={this.selectPhoto.bind(this)}>
                                <View style={[styles.img, styles.imgContainer, {alignItems:'center', marginTop: 30, marginBottom: 30}]}>
                                    {this.state.imageSource === null ?
                                        <Text>
                                            Selecione uma foto!
                                        </Text> :
                                        (<Image style={styles.img} source={this.state.imageSource}></Image>)
                                    }
                                </View>
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.cadastrarSenha}>Nome</Text>
                        <TextInput
                            editable = {true}
                            autoCapitalize = 'none'
                            style={styles.input}
                        >
                        </TextInput>

                        <Text style={styles.cadastrarSenha}>Data de nascimento</Text>
                        <TextInput
                            editable = {true}
                            autoCapitalize = 'none'
                            style={styles.input}
                        >
                        </TextInput>

                        <Text style={styles.cadastrarSenha}>Sexo?</Text>
                        <View style={[styles.input,{marginTop: 1}]}>
                            <RNPickerSelect
                                onValueChange={(value) => console.log(value)}
                                items={[
                                    { label: 'Masculino', value: 'Masculino' },
                                    { label: 'Feminino', value: 'Feminino' }
                                ]}
                            />
                        </View>

                        <Text style={styles.cadastrarSenha}>Raça?</Text>
                        <TextInput
                            secureTextEntry= {true}
                            editable = {true}
                            autoCapitalize = 'none'
                            style={styles.input}
                        >
                        </TextInput>
                        <Text style={styles.cadastrarSenha}>Tipo de pelo</Text>
                        <TextInput
                            secureTextEntry= {true}
                            editable = {true}
                            autoCapitalize = 'none'
                            style={styles.input}
                        >
                        </TextInput>


                        <TouchableHighlight onPress={() => {
                            this.props.navigation.replace('MainScreen')
                        }} style={styles.cadastrar}>
                            <Text style={styles.cadastrarText}>Salvar</Text>
                        </TouchableHighlight>
                    </View>
                </ScrollView>
            </ImageBackground>
        )
    }
}

export default withNavigation(CadastraBichinhoScreen);

const styles = StyleSheet.create({

    imgContainer: {
        backgroundColor: 'white',
        borderColor: '#000',
        borderWidth: 1 / PixelRatio.get(),
        justifyContent: 'center',
        alignItems: 'center'
    },
    img: {
        borderRadius: 75,
        width: 150,
        height: 150
    },
    input: {
        marginLeft: '10%',
        marginRight: '10%',
        marginBottom: 10,
        width: '80%',
        height: 50,
        paddingBottom: -20,
        marginTop: -20,
        borderBottomWidth: 1,
        borderBottomColor: 'black'
    },
    cadastrarSenha: {
        width: '80%',
        marginLeft: '10%',
        marginRight: '10%',
    },
    cadastrar: {
        marginLeft: '10%',
        marginRight: '10%',
        marginTop: 20,
        padding: 20,
        width: '80%',
        backgroundColor: 'grey',
        borderRadius: 5,
        borderWidth: 1,
        borderBottomColor: 'blue',
    },
    cadastrarText: {
        color: 'white',
        textAlign: 'center'
    },
    flexConta:{
        marginLeft: '10%',
        marginTop: 30,
        flex: 1,
        flexDirection: 'row'
    },
    possuiConta: {
        color: 'grey',
    },
    login: {
        textAlign: 'right',
        marginLeft: '10%',
        color: '#4C2104'
    }
})
